**Note:** This setup is now (2022) superseded by the one in https://gitlab.physik.uni-muenchen.de/Nikolai.Hartmann/xcache-nspawn-lrz

# Xcache @ LRZ

This repo tracks the definition file of the singularity container and additional configuration for the xcache image used at LRZ in Munich. It's based on Wei's instructions on github:

https://github.com/wyang007/rucioN2N-for-Xcache


# Some instructions by Guenter
(Aug 7, 2018)

See also

https://wiki.physik.uni-muenchen.de/etp/index.php/Xcache_stuff

created `/etc/xrootd/auth_db` following  https://twiki.cern.ch/twiki/bin/view/AtlasComputing/Tier3gXrootdSetup

created `/etc/grid-security/xrd/*` following https://twiki.cern.ch/twiki/bin/view/AtlasComputing/FAXproxyNew

## build new image

Before doing that, yum needs to be installed on the system and the file `~/.rpmmacros` needs to be created with the following content:

```
%_var /var
%_dbpath %{_var}/lib/rpm
```

The image is build with

```
singularity build "xcache.$(date +%F).img" xcache.singularity.def
```

make it used by systemd

```
ln -s "xcache.$(date +%F).img" xcache.img
```

## start shell w/ xcache

```
singularity shell -B /etc/xrootd:/etc/xrootd:ro -B /etc/grid-security:/etc/grid-security:ro -B /srv/xcache:/data:rw ~xcache/xcache.img
```

## remove cached files

```
./rm_cached_file.sh AOD.07709524._000001
```

will output files to be deleted and asks for confimation


## grid-proxy

regularly update via

```
/etc/cron.hourly/update_xcache-proxy
```

uses this as long term base-proxy

```
/root/.globus/base.proxy
```

create with

```
baseproxy=$HOME/.globus/base.proxy
grid-proxy-init -cert usercert.pem -key userkey.pem -out $baseproxy -valid 10000:00
```

# Setup git directory on server and push changes

```
mkdir git-config
cd git-config
git init
git config receive.denyCurrentBranch updateInstead
```

Then locally add remote and push to server, e.g. (assuming xcache1 is in ssh config)

```
git remote add xcache1 xcache1:git-config
git push xcache1 <branch-name>
```

If the pushed branch is checked out on the server and there are no untracked
changes this will update the files on the server.

On the server, set the worktree to `/`

```
# optionally check out branch, if not default
git checkout <branch-name>
# set worktree to /
git config core.worktree /
# don't track README on the server
git update-index --assume-unchanged /README.md
# clear files, except .git
rm -r *
```

From now on, pushes to the server will update files in-place (if there are no
untracked modifications).

Check status on server, ignoring untracked files
```
git status -uno
```

Optionally one can commit on the server and pull the changes down to the local
repository. But for that one needs to configure a user name and email (can only be one at at time).

```
git config user.name "<user-name>"
git config user.email "<user-email>"
```
