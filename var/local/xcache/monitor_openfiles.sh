#!/bin/bash

output_file="log_openfiles.txt"

while [ 1 ]
do
    process_id=$(ps aux | grep '/usr/bin/xrootd' | grep xcache  | awk '{print $2}')
    open_sockets=$(lsof -p $process_id | grep TCP | wc -l)
    open_files=$(lsof -p $process_id | grep "/data" | wc -l)
    open_all=$(lsof -p $process_id | wc -l)
    echo $(date +%s) ${process_id} ${open_sockets} ${open_files} ${open_all} >> ${output_file}
    sleep 20
done
